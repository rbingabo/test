export class Product {
  id: number;
  name: string;
  price: string;
  details: string;
  manufacturingDate: boolean;
}
