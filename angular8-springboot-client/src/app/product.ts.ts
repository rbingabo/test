export class Product {
    id: number;
    name: string;
    details: string;
    price: string;
    manufacturingDate: boolean;
}
